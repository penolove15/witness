########################
Eyewitness Documentation
########################

**Eyewitness** is light weight framework for object detection application

`naive schema <https://drive.google.com/file/d/1x_sCFs91swHR1Z3ofS4e2KFz6TK_kcHb/view?usp=sharing>`_


********
Contents
********

.. toctree::
   :maxdepth: 2
   :titlesonly:

   quick_start
   image_id
   image_utils
   object_detector
   detection_utils
   detection_result_filter
   evaluation
   audience_id
   feedback_msg_utils
   handlers_example
   db_models
   dataset
   mot
