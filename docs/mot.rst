MOT Module
===========
Modules related to MOT(multiple object tracking)


Video abstract
--------------
.. automodule:: eyewitness.mot.video
    :members:
    :undoc-members:
    :show-inheritance:


Tracker abstract
----------------
.. automodule:: eyewitness.mot.tracker
    :members:
    :undoc-members:
    :show-inheritance:


Evaluation
----------
.. automodule:: eyewitness.mot.evaluation
    :members:
    :undoc-members:
    :show-inheritance:


Visualize
---------
.. automodule:: eyewitness.mot.visualize_mot
    :members:
    :undoc-members:
    :show-inheritance:
