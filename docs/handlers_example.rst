Handler Example
===============
Handlers for detection results

DB Writer
---------

.. automodule:: eyewitness.result_handler.db_writer
    :members:
    :undoc-members:
    :show-inheritance:
