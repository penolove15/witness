Detection Result Filter
=======================
Utils modules used for filter detection result


.. automodule:: eyewitness.detection_result_filter
    :members:
    :undoc-members:
    :show-inheritance:
