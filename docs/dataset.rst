DataSet Utils
=============
used to export the detected data which can used for retrain/fine tune the model

.. automodule:: eyewitness.dataset_util
    :members:
    :undoc-members:
    :show-inheritance:
