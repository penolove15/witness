# Changelog

## [1.0.8] - 2019-04-14
- add progressing information while evaluation

## [1.0.7] - 2019-03-20
- fix the unsafe image_id from_str method, and more reasonable unmatched pattern handling 

## [1.0.6] - 2019-03-20
- fix the dataset generate_train_test_list string matching bugs, and add unit-test

## [1.0.5] - 2019-03-19
- fix the image_id initialization for looping over dataset objects

## [1.0.3] - 2019-03-18
- add Logging info for create_bbox_dataset_from_eyewitness
- add auto_image_registration mechanism for detection handler

## [1.0.2] - 2019-03-11
- add ObjectsClassifier which used to classify detected objs
- add arcface face recognition example

## [1.0.0] - 2019-02-26
- major modules for 1.0.0:
  - ImageId
  - Image
  - ObjectDetector
  - DetectionResult
  - DetectionResultHandler
  - DetectionResultFilter
  - VOC style DataSet
  - BboxMAPEvaluator
  - AudienceId
  - AudienceRegister
  - FeedbackMsg
  - FeedbackMsgHandler
  - ORM DB Models

## [0.31.0] - 2018-10-31
### Added
- add python 2.7 support(remove type hint, meta class with six, remove tempfile usage)


## [0.30.0] - 2018-10-28
### Added
- Add ORM DB handler example(sqlite) with peewee 
 