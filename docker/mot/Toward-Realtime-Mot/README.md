# eyewitness with MOT with Towards-Realtime-MOT

https://github.com/penolove/Towards-Realtime-MOT branch: eyeWitnessWrapper

this is a fork from https://github.com/Zhongdao/Towards-Realtime-MOT, and a wrapper example with eyewitness 


## pre-requirement
- nvidia-docker


## git clone repo and built images

1. git clone repo
```bash
git clone https://github.com/penolove/Towards-Realtime-MOT -b eyeWitnessWrapper
```

2. download weight and prepare demo video
for this demo we chose:
JDE-1088x608-uncertainty: [[Google Drive]](https://drive.google.com/open?id=1nlnuYfGNuHWZztQHXwVZSL_FvfE551pA) [[Baidu NetDisk]](https://pan.baidu.com/s/1Ifgn0Y_JZE65_qSrQM2l-Q)

demo video: https://pixabay.com/videos/people-commerce-shop-busy-mall-6387/


3. [optional] build the image
```
cd Towards-Realtime-MOT;
docker build -t penolove/towards-realtime-mot docker/
```


## run up container and run the inference
```bash
# mount your current folder into container
docker run --rm --gpus all -v $(pwd)/:/Towards-Realtime-MOT -ti penolove/towards-realtime-mot /bin/bash

# inside container
cd /Towards-Realtime-MOT;
python naive_tracker.py --input-video path/to/your/input/video --weights path/to/model/weights \
               --output-video path/to/your/output/video
```

and you can see a generated video file at path/to/your/output/video



## celery tracker example

### consumer
```bash
cd celery;
docker-compose up;  # make sure your docker "default-runtime": "nvidia"
```

### producer
```python
import os
from celery import Celery

BROKER_URL = os.environ.get('broker_url', 'amqp://guest:guest@192.168.1.177:5672')
celery = Celery('tasks', broker=BROKER_URL)

input_video = '/nfs/your_input.mp4'
output_video = '/nfs/your_output.mp4'
celery.send_task('track_video', args=[
    {'input_video': input_video, 'output_video': output_video}], queue='tracking')
```
