# Docker examples
MOT(multiple object tracking) docker examples with eyewitness


## MOT examples

### pytorch

- [Towards-Realtime-MOT](https://github.com/Zhongdao/Towards-Realtime-MOT)

### tensorflow

- [deep_sor_yolov3](https://github.com/Qidian213/deep_sort_yolov3)
