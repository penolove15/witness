# eyewitness with MOT with Towards-Realtime-MOT

https://github.com/penolove/deep_sort_yolov3 branch: eyeWitnessWrapper

this is a fork from https://github.com/Qidian213/deep_sort_yolov3, and a wrapper example with eyewitness 


## pre-requirement
- nvidia-docker


## git clone repo and built images

1. git clone repo
```bash
git clone https://github.com/penolove/deep_sort_yolov3 -b eyeWitnessWrapper
```

2. download weight and prepare demo video

[model weight yolo.h5](https://drive.google.com/file/d/1uvXFacPnrSMw6ldWTyLLjGLETlEsUvcE/view?usp=sharing)

demo video: https://pixabay.com/videos/people-commerce-shop-busy-mall-6387/

please put the yolo.h5 into deep_sort_yolov3/model_data/


3. [optional] build the image
```
cd deep_sort_yolov3;
docker build -t deep_sort_yolov3 docker/
```


## run up container and run the inference
```bash
# mount your current folder into container
docker run --rm --gpus all -v $(pwd)/:/deep-sort-yolov3 -ti penolove/deep-sort-yolov3 /bin/bash

# inside container
cd /deep-sort-yolov3;
python naive_tracker.py --input-video path/to/your/input/video --output-video path/to/your/output/video

# you can edit the model_image_size in yolo.py to detect a smaller objects 
```

and you can see a generated video file at path/to/your/output/video


