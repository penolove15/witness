# Traefik example 

actually this docker-compose.yml is not related to this repository,
this is just a example for wrappering our monitor-reporter with traefik
the main purpose is re-direct traffic to our container and let's encrypt setup.

## run it up.
```
docker-compose up -d
```

https://ooxx.docker.localhost/admin/


## setup your real ssl cer, crt, and site-domain 
in toml file and Dockerfile(site-domain above(ooxx.docker.localhost), and cer, crt files)
