# Yolov3 implementation with pytorch

https://github.com/penolove/yolov3 branch: eyeWitnessWrapper

this is a fork from https://github.com/ultralytics/yolov3, and wrapper with eyewitness 

## pre-requirement
- install [docker](https://www.docker.com/) / [nvidia-docker](https://github.com/NVIDIA/nvidia-docker)

### create folders for demo
```
mkdir weights
mkdir detected_image
```

### download model weight
```
wget https://pjreddie.com/media/files/yolov3.weights -P weights/
```


## start container and make a inference
```bash
docker run --name yolov3 \
           --gpus all \
           -v $PWD/detected_image:/workspace/yolov3/detected_image \
           -v $PWD/weights:/workspace/yolov3/weights \
           -d penolove/pytorch_yolo_v3:gpu \
           tail -f /dev/null;

# if you want to call train.py please pass
# --ipc host \ 

# access container
docker exec -ti yolov3 /bin/bash
cd /workspace/yolov3;
# the detected 183 club image will be stored in detected_image
python naive_detector.py --cfg cfg/yolov3.cfg --weight_file weights/yolov3.weights
```

## Build image from Dockerfile:
- gpu
```
docker build --no-cache -t penolove/pytorch_yolo_v3:gpu gpu/
```



