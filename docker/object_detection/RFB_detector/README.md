# eyewitness with FSSD, RFB

https://github.com/penolove/PytorchSSD branch: eyeWitnessWrapper

this is a fork from https://github.com/lzx1413/PytorchSSD, and wrapper with eyewitness 

- FSSD FSSD: Feature Fusion Single Shot Multi-box Detector
- RFB-SSDReceptive Field Block Net for Accurate and Fast Object Detection


## 1. download [RFB model weights](https://drive.google.com/file/d/1GlxBi25ufGZGtHmdsXzigF7fqVJmg0rT) into weights folder, make required folders
```
mkdir weights
mkdir detected_image
# and download RFB300_80_5.pth into weights(e.g. weights/RFB300_80_5.pth , ~140MB)
```

## 2. Run built RFB docker image:

### cpu container (for gpu image is about ~ 1.6GB)
```
docker run --name pytorch_rfb \
           -v $PWD/weights:/PytorchSSD/weights \
           -v $PWD/detected_image:/PytorchSSD/detected_image \
           -d penolove/pytorch_rfb:cpu \
           tail -f /dev/null;

```

### gpu container (for gpu image is about ~ 4GB)

[installation nvidia-dcoker](https://github.com/NVIDIA/nvidia-docker)
```
# make suer your nvidia driver support cuda 9.0 (> 0.384.81, you can using nvidia-smi to check it)
https://docs.nvidia.com/cuda/cuda-toolkit-release-notes/index.html

nvidia-docker run --name pytorch_rfb \
                  -v $PWD/weights:/PytorchSSD/weights \
                  -v $PWD/detected_image:/PytorchSSD/detected_image \
                  -d penolove/pytorch_rfb:gpu \
                  tail -f /dev/null;
```

### run a detector example
```
# get into container
docker exec -ti pytorch_rfb /bin/bash;
# execute command in container
cd PytorchSSD; 
# for cpu
python naive_detector.py
# for gpu
python naive_detector.py --cuda True
```

## 3. End2End example with a webcam

### includes:
- image producer (mount the web-cam to container)
- rfb detector
- detection result handler
  - sqlite writer
  - line msg sender (set site-domain(not set will sent pikachu), line-channel-secret in env.list)

### [optional] mount the drawn_image folder, db_folder
```
mkdir detected_image  # used for store drawn_images
mkdir db_folder  # used for store sqlite db file
```

### First attach your webcam device to container

```bash
# cpu
# if the env.list not set, remove --env-file line
docker run --name pytorch_rfb \
           --device=/dev/video0 \
           --env-file env.list \
           -v $PWD/weights:/PytorchSSD/weights \
           -v $PWD/detected_image:/PytorchSSD/detected_image \
           -v $PWD/db_folder:/PytorchSSD/db_folder \
           -d penolove/pytorch_rfb:cpu \
           tail -f /dev/null;

# gpu
# if the env.list not set, remove --env-file line
nvidia-docker run --name pytorch_rfb \
                  --device=/dev/video0 \
                  --env-file env.list \
                  -v $PWD/weights:/PytorchSSD/weights \
                  -v $PWD/detected_image:/PytorchSSD/detected_image \
                  -v $PWD/db_folder:/PytorchSSD/db_folder \
                  -d penolove/pytorch_rfb:gpu \
                  tail -f /dev/null;
```

### Run end2end application

```
docker exec -ti pytorch_rfb /bin/bash
cd PytorchSSD
# using webcam as image source 
python end2end_detector.py --db_path db_folder/example.sqlite
# using webcam as image source gpu
python end2end_detector.py --db_path db_folder/example.sqlite  --cuda True
```



## Build image from Dockerfile:
### cpu
```
docker build --no-cache -t penolove/pytorch_rfb:cpu cpu/
```

### gpu
```
# for rebuild the image, check the command(also cuda verison) with https://pytorch.org/
nvidia-docker build --no-cache -t penolove/pytorch_rfb:gpu gpu/
```
