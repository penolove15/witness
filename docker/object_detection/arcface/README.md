# face recognition with ArcFace

https://github.com/penolove/insightface branch: eyeWitnessWrapper

this is a fork from https://github.com/deepinsight/insightface , and a wrapper example with eyewitness 

## pre-requirement
1. download pre-trained model [LResNet100E-IR,ArcFace@ms1m-refine-v2.zip](https://www.dropbox.com/s/tj96fsm6t6rq8ye/model-r100-arcface-ms1m-refine-v2.zip?dl=0) into weights folder. (weights/model-r100-ii)

[google-drive backup] ()if the link not work, 

2. make required folder
```
mkdir weights
mkdir detected_image

# unzip downloaded folder
unzip weights/model-r100-arcface-ms1m-refine-v2.zip -d weights

```


## Quick Start - naive_detector
### CPU
```bash
# cpu (image about ~ 1.6GB)
docker run --name arcface \
           -v $PWD/weights:/insightface/models \
           -v $PWD/detected_image:/insightface/detected_image \
           -d penolove/arcface:cpu \
           tail -f /dev/null;

# a detection(mtcnn) + classification(arcface with 183club) example
docker exec -ti arcface /bin/bash
cd /insightface
python3 naive_detector.py --gpu -1
```


#### GPU
```bash
# gpu (image about ~ 4GB)
docker run --name arcface \
           --gpus all \
           -v $PWD/weights:/insightface/models \
           -v $PWD/detected_image:/insightface/detected_image \
           -d penolove/arcface:gpu \
           tail -f /dev/null;

# a detection(mtcnn) + classification(arcface with 183club) example
docker exec -ti arcface /bin/bash
cd /insightface
python3 naive_detector.py --gpu 0
```


## create your own face classifier(face.pkl): feature extraction, labeling 
1. start container
```
docker run --name arcface \
           -v $PWD/weights:/insightface/models \
           -v $PWD/detected_image:/insightface/detected_image \
           -d penolove/arcface:cpu \
           tail -f /dev/null;
```

2. detect faces (mtcnn) in your own images, (following I use the demo/183club/*.jpg)
```
# detect faces
docker exec -ti arcface /bin/bash
cd insightface
python mtcnn_example.py --db_path 183.sqlite --input_image_path demo/183club --gpu -1

# extract as dataset(voc style)
python generate_face_dataset.py --db_path 183.sqlite --output_dataset_path models/183
```

3. you can edit the label of dataset by [labelImg](https://github.com/tzutalin/labelImg)


4. convert dataset into face.pkl which contains face embedding
```
# store face embedding into face.pkl
python mtcnn_arcface_classifier.py \
  --dataset_folder models/183 \
  --dataset_embedding_path models/face.pkl \
  --gpu -1

# recognize with your own image with face.pkl
python mtcnn_arcface_classifier.py \
  --dataset_embedding_path models/face.pkl \
  --demo_image demo/183club/test_image2.jpg \
  --drawn_image_path detected_image/demo.jpg
  --gpu -1
```

## end2end example:
contains
- webcam producer
- face detector mtcnn
- face classifier(arcface with face.pkl generated above)

```
# cpu
# if the env.list not set, remove --env-file line
docker run --name arcface \
           --device=/dev/video0 \
           --env-file env.list \
           -v $PWD/weights:/insightface/models \
           -v $PWD/detected_image:/insightface/detected_image \
           -v $PWD/db_folder:/insightface/db_folder \
           -v $PWD/raw_image:/insightface/raw_image \
           -d penolove/arcface:cpu \
           tail -f /dev/null;

# gpu
# if the env.list not set, remove --env-file line
docker run --name arcface \
           --gpus all \
           --device=/dev/video0 \
           --env-file env.list \
           -v $PWD/weights:/insightface/models \
           -v $PWD/detected_image:/insightface/detected_image \
           -v $PWD/db_folder:/insightface/db_folder \
           -v $PWD/raw_image:/insightface/raw_image \
           -d penolove/arcface:gpu \
           tail -f /dev/null;
```

```
# access container
docker exec -ti arcface /bin/bash;

# alias python=python3  # if gpu model

python end2end_detector.py \
    --db_path db_folder/example.sqlite \
    --raw_image_folder raw_image \
    --dataset_embedding_path models/face.pkl
    # --gpu 0  # if gpu, use gpu id: 0
```

## End2End Example with docker-compose
- webcam image producer
- a container for UI
- a container for detector
```
cd cpu; 
# cd gpu
docker-compose up;
```

## Flask Server Example with docker-compose
```
cd cpu/flask-server;
#cd gpu/flask-server;
docker-compose up;
```


```
wget https://upload.wikimedia.org/wikipedia/commons/2/25/5566_and_Daily_Air_B-55507_20050820.jpg -O raw_image/5566.jpg

curl -X POST \
  http://localhost:5566/detect_post_path \
  -H 'content-type: application/json' \
  -H 'image_id: 5566--1541860159--jpg' \
  -H 'raw_image_path: /insightface/raw_image/5566.jpg'
```


## Build image from Dockerfile:

### cpu
```bash
docker build --no-cache -t penolove/arcface:cpu cpu/
```


### gpu
```bash
docker build --no-cache -t penolove/arcface:gpu gpu/
```
