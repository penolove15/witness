# Yolov3 implementation with TensorRT

https://github.com/penolove/yolov3-tensorrt branch: eyeWitnessWrapper

this is a fork from https://github.com/xuwanqi/yolov3-tensorrt, and wrapper with eyewitness 

actually the origin repo from xuwanqi is just the same as example from JetPack example 
https://docs.nvidia.com/deeplearning/sdk/tensorrt-sample-support-guide/index.html

which transform the yolov3 [darknet implementation](https://pjreddie.com/darknet/yolo/) -> onnx -> tensorRT


## prepare yolov3 weights
```bash
mkdir weights
mkdir detected_image
mkdir raw_image
mkdir db_folder  # used for end2end example
wget -O weights/yolov3.weights https://pjreddie.com/media/files/yolov3.weights
```

## Transform yolov3 model into TensorRT engine

start and access container first
```bash
# start container
docker run \
    --name yolov3_trt \
    --gpus all \
    -v $PWD/weights:/workspace/yolov3-tensorrt/weights \
    -v $PWD/detected_image:/workspace/yolov3-tensorrt/detected_image \
    -d  penolove/tensorrt_yolo_v3:gpu \
    tail -f /dev/null;

# access into container
docker exec -ti yolov3_trt /bin/bash

# convert model into tensorRT engine
cd yolov3-tensorrt/;
python yolov3_to_onnx.py  # convert darknet into onnx
python onnx_to_tensorrt.py  # build tensorRT engine and store it to yolov3.engine
```

## naive_example: using eyewitness to wrapper the classifier
```bash
# make sure the yolov3.engine were generated
python naive_detector.py --engine_file yolov3.engine
```

## detector flask
```bash
# make sure the yolov3.engine were generated
python detector_with_flask.py  \
    --engine_file yolov3.engine \
    --db_path db_folder/example.sqlite \
    --drawn_image_dir detected_image \
    --detector_host 0.0.0.0
```

```bash
# post a 56 image 
wget https://upload.wikimedia.org/wikipedia/commons/2/25/5566_and_Daily_Air_B-55507_20050820.jpg -O raw_image/5566.jpg

curl -X POST \
  http://localhost:5566/detect_post_path \
  -H 'content-type: application/json' \
  -H 'image_id: 5566--1541860142--jpg' \
  -H 'raw_image_path: /workspace/yolov3-tensorrt/raw_image/5566.jpg'
```

## celery detector worker example 
0. start celery work
```
cd gpu/celery;
# start a celery woker and rabbitmq server.
# this will build the engine, so it will take times to start celery worker
# you can edit the compose file to figure out the best concurrency setting, worker amount
# for your device
docker-compose up -d
```

1. access the yolov3_trt container and sent a celery task
```
docker-compose exec yolov3_trt /bin/bash
```

2. sent image task
```python
import arrow
from celery_tasks import detect_image
params = {
  'channel': '5566',
  'timestamp': arrow.now().timestamp,
  'is_store_detected_image': True,
}
detect_image.si(params).apply_async()
```


## Build image from Dockerfile:
- gpu
```
docker build --no-cache -t penolove/tensorrt_yolo_v3:gpu gpu/
```


## [jetson_Nano  JetPack 4.2.2] celery consumer example

in this example provide a celery consumer example, and requires:
- rabbitmq server
- [yolov3 tensorrt engine](https://drive.google.com/drive/folders/1Z5X1HlixqwYCoK8C2D-51r826c1rTrT3?usp=sharing)

[optional] built your own the yolov3 tensorrt engine:
```
# repo https://github.com/penolove/yolov3-tensorrt.git branch: eyeWitnessWrapper
# cd /yolov3-tensorrt;
# download the weights
mkdir weights
wget -O weights/yolov3.weights https://pjreddie.com/media/files/yolov3.weights
# convert darknet weight into onnx format
python3 yolov3_to_onnx.py
# edit the following line in the onnx_to_tensorrt.py 
# builder.max_workspace_size = 1 << 28  # ori 30(1GB)->28(256MB)
# to reduce gpu mem usage from 1GB to 256MB
# this script already detect a example image
python3 onnx_to_tensorrt.py
```

[optional] in your jetson nano build image
```
docker build --no-cache -t jetson_nano_tensorrt_yolov3 jetson_nano/
```

set your engine/rabbitmq/db setting in jetson_nano/celery/docker-compose.yml, e.g.
```
- inference_shape=608,608
- threshold=0.14
- valid_labels=person,cup
- engine_file=/nfs/eyewitness/yolov3.engine
- raw_image_fodler=/nfs/eyewitness/raw_image/
- detected_image_folder=/nfs/eyewitness/detected_image/
- db_path=/nfs/eyewitness/db_path/example.sqlite
- broker_url=amqp://guest:guest@192.168.0.107:5672
```

start container
```
cd jetson_nano/celery;
docker-compose up -d 
# for scale up you can set --scale yolov3_trt=2
```
