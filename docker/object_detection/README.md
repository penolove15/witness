# Docker examples
object detector docker examples with eyewitness:

more details and Dockerfile for both cpu, gpu were stores in their folders

## [optional] Docker compose with GPU
Notice that as wanted to used gpu with docker-compose 
before the request haven't come up a conclusion https://github.com/docker/compose/issues/6691.

we will use `nvidia-container-runtime` as a workaround
please install [nvidia-container-runtime](https://github.com/NVIDIA/nvidia-container-runtime)
```
sudo apt-get install nvidia-container-runtime
```

edit setting
```
# edit/ create  /etc/docker/daemon.json with content
{
    "runtimes": {
        "nvidia": {
            "path": "/usr/bin/nvidia-container-runtime",
            "runtimeArgs": []
        }
    },
    "default-runtime": "nvidia"
}
```

restart dockerd
```
sudo pkill -SIGHUP dockerd
```

---

## examples
### pytorch
- [RFB-SSD](https://github.com/lzx1413/PytorchSSD) - ECCV 2018
- [YoloV3](https://github.com/ultralytics/yolov3)
- [CenterNet](https://github.com/xingyizhou/CenterNet)

### keras
- [YoloV3](https://github.com/qqwweee/keras-yolo3)

### caffe
- [RefineDet](https://github.com/sfzhang15/RefineDet) - CVPR 2018
- [Pelee](https://github.com/Robert-JunWang/Pelee) - NIPS 2018
- [mtcnn](https://github.com/DuinoDu/mtcnn) - {IEEE} Signal Processing Letters 2016

### mxnet
- [Trident](https://github.com/TuSimple/simpledet)
- [arcface](https://github.com/deepinsight/insightface) - CVPR 2019

### TensorRT
- [YoloV3](https://github.com/xuwanqi/yolov3-tensorrt)


### cv2_detector
- [cv2_detector]()

---

## simple UI support:
https://github.com/penolove/Flask-Monitor-Reporter
you can mount the drawn images, db folders to the flask-monitor container.
a docker-compose example can be found at docker/YOLOv3_detector/docker-compose.yml


## [for developer] add a new detector example 
1. a repo with:
    - naive_detector.py: need to implement a ObjectDetector 
    - end2end_detector.py: need to implement image producer, use ObjectDetector from naive_detector, ResultHandlers
    - detector_with_flask.py: wrapper ResultHandler, ObjectDetector into BboxObjectDetectionFlaskWrapper

2. provide docker files (dockerfile, docker-compose): cpu, gpu, 
