# eyewitness with Pelee

https://github.com/penolove/Pelee branch: eyeWitnessWrapper

this is a fork from https://github.com/Robert-JunWang/Pelee, and a wrapper example with eyewitness 


## pre-requirement
- install docker / nvidia-docker(for gpu)
- [optional] set site-domain(url prefix for static drawn image), line channel access token env varialbe in env.list
- [optional] docker-compose

### 1. download pre-trained model [pelee_merged.tar.gz](https://drive.google.com/open?id=1AH46csPPufZl3NYwk6xcHDmnYVwmN05e) into weights folder, make required folders
```
mkdir weights
mkdir detected_image
mkdir db_folder
tar xvzf pelee_merged.tar.gz -C weights/
```

## Quick Start
### Run from built docker image:
1. start container
```
# cpu (image about ~ 1.5GB)
docker run --name pelee \
           -v $PWD/weights:/opt/caffe/examples/pelee/model \
           -v $PWD/detected_image:/opt/caffe/detected_image \
           -d penolove/pelee:cpu \
           tail -f /dev/null;


# gpu (for gpu image is about ~ 3.5GB)
docker run --name pelee \
           --gpus all \
           -v $PWD/weights:/opt/caffe/examples/pelee/model \
           -v $PWD/detected_image:/opt/caffe/detected_image \
           -d penolove/pelee:gpu \
           tail -f /dev/null;
```


2. access container
```bash
docker exec -ti pelee /bin/bash

```

3. run prediction example
```
cd /opt/caffe/; 
# cpu
python examples/pelee/naive_detector.py --gpu_id -1

# gpu
# python examples/pelee/naive_detector.py
```


## End2End example with a webcam
- image producer (mount the webcam to container)
- PeLee detector
- detection result handler
  - sqlite writer
  - line msg sender (set site-domain(not set will sent pikachu), channel access token in env.list)

1. start container, attach your webcam device to container

```bash
# cpu
# if the env.list not set, remove --env-file line
docker run --name pelee \
           --device=/dev/video0 \
           --env-file env.list \
           -v $PWD/weights:/opt/caffe/examples/pelee/model \
           -v $PWD/detected_image:/opt/caffe/detected_image \
           -v $PWD/db_folder:/opt/caffe/db_folder \
           -d penolove/pelee:cpu \
           tail -f /dev/null;

# gpu
# if the env.list not set, remove --env-file line
docker run --name pelee \
           --gpus all \
           --device=/dev/video0 \
           --env-file env.list \
           -v $PWD/weights:/opt/caffe/examples/pelee/model \
           -v $PWD/detected_image:/opt/caffe/detected_image \
           -v $PWD/db_folder:/opt/caffe/db_folder \
           -d penolove/pelee:gpu \
           tail -f /dev/null;
```

2. Run end2end application, read image from webcam

```
docker exec -ti pelee /bin/bash

cd /opt/caffe/
# cpu
python examples/pelee/end2end_detector.py --db_path db_folder/example.sqlite --gpu_id -1

# gpu
# python examples/pelee/end2end_detector.py --db_path db_folder/example.sqlite
```


## RUN the docker-compose example
- two container:
  - pelee: pelee detector, webcam producer, result handler for [db, line channel]
  - monitor-reporter: UI for showing detected result, webhook for line 

- pre-requirement:
  - clean db file(avoid registered audiences inside db file)
  - make sure your webcam at /dev/video0 (or edit docker-compose.yml)
  - [optional] line channel developer (set your setting in env.list)
    - webhook entry point at localhost:5000/line_feedback/callback
      - need a http re-direction to https (ngrok example: https://d31ef5b3.ngrok.io/line_feedback/callback)
      - set line channel webhook url to the https url above.

- cpu
```
# cpu
cd cpu;
docker-compose up 
```

- gpu
```
# gpu
cd gpu;
docker-compose up
```


you can see a UI at:
- bar_chart: http://localhost:5000/bar_chart/
- db_info: http://localhost:5000/admin/

[optional] Line notification
- say 'open the door' from your line to channel (do the register)
- detector will sent line msg to you as person detected


## calculate mAP with VOC2007 dataset
```
download the VOC dataset first:
```
Download the training, validation, test data and VOCdevkit

wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar
wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar
wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCdevkit_08-Jun-2007.tar
```

we will only use the VOC2007 folder:
```
VOC2007/Annotations
VOC2007/ImageSet
VOC2007/JPGImages
```

1. start container
```
# cpu (~ 1.5 GB)
docker run --name pelee \
           -v $PWD/weights:/opt/caffe/examples/pelee/model \
           -v $PWD/VOC2007:/opt/caffe/examples/pelee/VOC2007 \
           -d penolove/pelee:cpu \
           tail -f /dev/null;

# gpu (~ 3.5GB)
docker run --name pelee \
           --gpus all \
           -v $PWD/weights:/opt/caffe/examples/pelee/model \
           -v $PWD/VOC2007:/opt/caffe/examples/pelee/VOC2007 \
           -d penolove/pelee:gpu \
           tail -f /dev/null;
```

2. access container and run a evaluation process:
```
# [gpu] 
# which should lead to ~ 0.58
cd /opt/caffe/examples/pelee
python eyewitness_evaluation.py
```


## Build image from Dockerfile:
- cpu
```
# for rebuild the image for cpu
docker build --no-cache -t penolove/pelee:cpu cpu/
```

- gpu
```
# for rebuild the image for gpu
docker build --no-cache -t penolove/pelee:gpu gpu/
```
