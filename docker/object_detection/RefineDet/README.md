# eyewitness with RefineDet

https://github.com/penolove/RefineDet branch: eyeWitnessWrapper

this is a fork from https://github.com/sfzhang15/RefineDet, and a wrapper example with eyewitness 


## pre-requirement
- install docker / nvidia-docker(for gpu)
- [optional] set site-domain(url prefix for static drawn image), line channel access token env varialbe in env.list
- [optional] docker-compose

### 1. download 320 VOC pre-trained model [RefineDet model weights](https://drive.google.com/open?id=1rj6ikGCJg_rOlt0gUCXkjHzPkjpvzxXV) into weights folder, make required folders
```
mkdir weights
mkdir detected_image
mkdir db_folder
mkdir raw_iamge
tar xvzf models_VGGNet_VOC0712_refinedet_vgg16_320x320.tar.gz -C weights/
```

## QuickStart
### Run from built docker image:
1. start container
```
# cpu (image about ~ 1.5GB)
docker run --name refine_det \
           -v $PWD/weights/models:/opt/caffe/models \
           -v $PWD/detected_image:/opt/caffe/detected_image \
           -d penolove/refinedet:cpu \
           tail -f /dev/null;


# gpu (for gpu image is about ~ 3GB)
docker run --name refine_det \
           --gpus all \
           -v $PWD/weights/models:/opt/caffe/models \
           -v $PWD/detected_image:/opt/caffe/detected_image \
           -d penolove/refinedet:gpu \
           tail -f /dev/null;
```

2. access container
```bash
docker exec -ti refine_det /bin/bash

```

3. run prediction example
```
cd /opt/caffe/; 
python naive_detector.py --gpu_id -1

# gpu
# python naive_detector.py
```

## End2End example with a webcam
- image producer (mount the webcam to container)
- RefineDet detector
- detection result handler
  - sqlite writer
  - line msg sender (set site-domain(not set will sent pikachu), channel access token in env.list)

1. start container, attach your webcam device to container
```bash
# cpu
# if the env.list not set, remove --env-file line
docker run --name refine_det \
           --device=/dev/video0 \
           --env-file env.list \
           -v $PWD/weights/models:/opt/caffe/models \
           -v $PWD/detected_image:/opt/caffe/detected_image \
           -v $PWD/db_folder:/opt/caffe/db_folder \
           -d penolove/refinedet:cpu \
           tail -f /dev/null;

# gpu
# if the env.list not set, remove --env-file line
docker run --name refine_det \
           --gpus all \
           --device=/dev/video0 \
           --env-file env.list \
           -v $PWD/weights/models:/opt/caffe/models \
           -v $PWD/detected_image:/opt/caffe/detected_image \
           -v $PWD/db_folder:/opt/caffe/db_folder \
           -d penolove/refinedet:gpu \
           tail -f /dev/null;
```

2. Run end2end application

```
# access container
docker exec -ti refine_det /bin/bash

cd /opt/caffe/
# [cpu] using webcam as image source 
python end2end_detector.py --db_path db_folder/example.sqlite --gpu_id -1

# [gpu] using webcam as image source 
# python end2end_detector.py --db_path db_folder/example.sqlite 
```


## RUN webcam end2end docker-compose example
- two container:
  - refine_det: refine_det detector, webcam producer, result handler for [db, line channel]
  - monitor-reporter: UI for showing detected result, webhook for line 

- pre-requirement:
  - clean db file(avoid registered audiences inside db file)
  - make sure your webcam at /dev/video0 (or edit docker-compose.yml)
  - [optional] line channel developer (set your setting in env.list)
    - webhook entry point at localhost:5000/line_feedback/callback
      - need a http re-direction to https (ngrok example: https://d31ef5b3.ngrok.io/line_feedback/callback)
      - set line channel webhook url to the https url above.

- cpu
```
# cpu
cd cpu;
docker-compose up 
```

- gpu
```
cd gpu;
docker-compose up
```


you can see a UI at:
- bar_chart: http://localhost:5000/bar_chart/
- db_info: http://localhost:5000/admin/

[optional] Line notification
- say 'open the door' from your line to channel (do the register)
- detector will sent line msg to you as person detected


## RUN docker flask detector docker-compose example

```
# gpu
cd gpu/flask-server;
docker-compose up
```

- post a image
```
wget https://upload.wikimedia.org/wikipedia/commons/2/25/5566_and_Daily_Air_B-55507_20050820.jpg -O raw_image/5566.jpg

curl -X POST \
  http://localhost:5566/detect_post_path \
  -H 'content-type: application/json' \
  -H 'image_id: 5566--1541860152--jpg' \
  -H 'raw_image_path: /opt/caffe/raw_image/5566.jpg'
```


## calculate mAP with VOC2007 dataset
```
download the VOC dataset first:
```
Download the training, validation, test data and VOCdevkit

wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar
wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar
wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCdevkit_08-Jun-2007.tar
```

we will only use the VOC2007 folder:
```
VOC2007/Annotations
VOC2007/ImageSet
VOC2007/JPGImages
```

1. start container
```
# cpu (~ 1.5 GB)
docker run --name refine_det \
           -v $PWD/weights/models:/opt/caffe/models \
           -v $PWD/VOC2007:/opt/caffe/VOC2007 \
           -d penolove/refinedet:cpu \
           tail -f /dev/null;

# gpu (~ 3.5GB)
docker run --name refine_det \
           --gpus all \
           -v $PWD/weights/models:/opt/caffe/models \
           -v $PWD/VOC2007:/opt/caffe/VOC2007 \
           -d penolove/refinedet:gpu \
           tail -f /dev/null;         
```

2. access container and run a evaluation process:
```
# [gpu] 
# which should lead to ~ 0.76
cd /opt/caffe/
python eyewitness_evaluation.py 
```




## Build image from Dockerfile:
- cpu
```
# for rebuild the image for cpu
docker build --no-cache -t penolove/refinedet:cpu cpu/
```

- gpu
```
# for rebuild the image for gpu
docker build --no-cache -t penolove/refinedet:gpu gpu/
```
