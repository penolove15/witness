# eyewitness with keras-yoloV3

https://github.com/penolove/keras-yolo3 branch: eyeWitnessWrapper

this is a fork from https://github.com/qqwweee/keras-yolo3, and wrapper with eyewitness 

## pre-requirement
- install [docker](https://www.docker.com/) / [nvidia-docker](https://github.com/NVIDIA/nvidia-docker)
- [optional] set site-domain (url prefix for static drawn image), line channel access token env varialbe in env.list
- [optional] docker-compose

### 1. create two folders for demo
```
mkdir weights
mkdir detected_image
mkdir raw_image
mkdir db_folder  # used for end2end example
```

### 2. download yolo model into weights folder
(e.g. weights/yolov3.weights, ~248MB)
```
wget https://pjreddie.com/media/files/yolov3.weights -P weights/

# or directly download a converted files from https://drive.google.com/drive/folders/1oXoSnpu7K7m6zyoTxVF1tgtGZ2_HjxGT?usp=sharing
```

## Quick Start
### Run from built docker image:
1. start container
```
# cpu (~ 2 GB)
docker run --name yolov3 \
           -v $PWD/weights:/keras-yolo3/weights \
           -v $PWD/detected_image:/keras-yolo3/detected_image \
           -d penolove/keras_yolo_v3:cpu \
           tail -f /dev/null;

# gpu (~ 4.5GB), gpu were built with nvidia cuda10 image, please check host nvidia driver version 
# here https://github.com/NVIDIA/nvidia-docker/wiki/CUDA#requirements 
# or you need to re-built the gpu image and downgrade the tensorflow version
docker run --name yolov3 \
           --gpus all \
           -v $PWD/weights:/keras-yolo3/weights \
           -v $PWD/detected_image:/keras-yolo3/detected_image \
           -d penolove/keras_yolo_v3:gpu \
           tail -f /dev/null;
```

2. access container
```bash
docker exec -ti yolov3 /bin/bash
```

3. convert model weight for keras
```
cd keras-yolo3;

# gpu
# alias python=python3

python convert.py yolov3.cfg weights/yolov3.weights model_data/yolo.h5

# or directly cp h5 to model_data
# cp weights/yolo.h5 model_data/
```

4. run prediction example
```
# gpu
# alias python=python3

python naive_detector.py

# since there are some overhead for tensorflow, it want to show speed difference,
# please edit naive_detector with more detection iterations.
```

## End2End example with a webcam
- image producer (mount the webcam to container)
- yolov3 detector
- detection result handler
  - sqlite writer
  - line msg sender (set site-domain(not set will sent pikachu), channel access token in env.list)

1. start container, attach your webcam device to container

```bash
# cpu
# if the env.list not set, remove --env-file line
docker run --name yolov3 \
           --device=/dev/video0 \
           --env-file env.list \
           -v $PWD/weights:/keras-yolo3/weights \
           -v $PWD/detected_image:/keras-yolo3/detected_image \
           -v $PWD/db_folder:/keras-yolo3/db_folder \
           -d penolove/keras_yolo_v3:cpu \
           tail -f /dev/null;

# gpu
# if the env.list not set, remove --env-file line
docker run --name yolov3 \
           --gpus all \
           -v $PWD/weights:/keras-yolo3/weights \
           -v $PWD/detected_image:/keras-yolo3/detected_image \
           -v $PWD/db_folder:/keras-yolo3/db_folder \
           -d penolove/keras_yolo_v3:gpu \
           tail -f /dev/null;
```

2. Run end2end application

```
docker exec -ti yolov3 /bin/bash
# [gpu] 
# alias python=python3

cd keras-yolo3
# convert model weight
python convert.py yolov3.cfg weights/yolov3.weights model_data/yolo.h5

# or just copy the h5
# cp weights/yolo.h5 model_data/

# using webcam as image source
python end2end_detector.py --db_path db_folder/example.sqlite
```


## RUN the docker-compose example
- two container:
  - yolov3: yolov3 image detector, webcam image producer, result handler for db, line channel
  - monitor-reporter: UI for showing detected result, webhook for line 

- pre-requirement:
  - clean db file(avoid registered auidences inside db file)
  - make sure your webcam at /dev/video0 (or edit docker-compose.yml)
  - make sure you have download the yolo.h5 and put in weights (avoid convert again)
  - [optional] line channel developer (set your setting in env.list)
    - webhook entry point at localhost:5000/line_feedback/callback
      - need a http re-direction to https (ngrok example: https://d31ef5b3.ngrok.io/line_feedback/callback)
      - set line channel webhook url to the https url above.

- cpu
```
# cpu
cd cpu;
docker-compose up 
```

- GPU
```
# gpu, docker-compose with format 2.3 which support the runtime option
# https://docs.docker.com/compose/compose-file/compose-file-v2/#runtime
cd gpu;
docker-compose up
```

you can see a UI at:
- bar_chart: http://localhost:5000/bar_chart/
- db_info: http://localhost:5000/admin/

[optional] Line notification
- say 'open the door' from your line to channel (do the register)
- detector will sent line msg to you as person detected



## RUN docker flask detector docker-compose example
- gpu
```
cd gpu/flask-server;
docker-compose up
```

- post a image 
```
wget https://upload.wikimedia.org/wikipedia/commons/2/25/5566_and_Daily_Air_B-55507_20050820.jpg -O raw_image/5566.jpg

curl -X POST \
  http://localhost:5566/detect_post_path \
  -H 'content-type: application/json' \
  -H 'image_id: 5566--1541860142--jpg' \
  -H 'raw_image_path: /keras-yolo3/raw_image/5566.jpg'

# thus you can check the result in detected_image
```


## calculate mAP with VOC2007 dataset
download the VOC dataset first:
```
Download the training, validation, test data and VOCdevkit

wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar
wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar
wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCdevkit_08-Jun-2007.tar

tar xvf VOCtrainval_06-Nov-2007.tar
tar xvf VOCtest_06-Nov-2007.tar
tar xvf VOCdevkit_08-Jun-2007.tar
```

we will only use the VOC2007 folder:
```
VOC2007/Annotations
VOC2007/ImageSet
VOC2007/JPGImages
```

1. start container
```
# cpu (~ 2 GB)
docker run --name yolov3 \
           -v $PWD/weights:/keras-yolo3/weights \
           -v $PWD/VOC2007:/keras-yolo3/VOC2007 \
           -d penolove/keras_yolo_v3:cpu \
           tail -f /dev/null;

# gpu (~ 4.5GB)
docker run --name yolov3 \
           --gpus all \
           -v $PWD/weights:/keras-yolo3/weights \
           -v $PWD/VOC2007:/keras-yolo3/VOC2007 \
           -d penolove/keras_yolo_v3:gpu \
           tail -f /dev/null;                  
```

2. access container and run a evaluation process:
```
# [gpu] 
# alias python=python3
# which should lead to ~0.73 mAP
python eyewitness_evaluation.py 
```


## celery example
```
cd cpu/celery;
docker-compose up
# which exec
# celery -A celery_tasks worker --loglevel=debug --concurrency=1  --pool=gevent
```

sent task to 
```python
import os
from celery import Celery
BROKER_URL = os.environ.get('broker_url', 'amqp://guest:guest@rabbitmq:5672')
celery = Celery('tasks', broker=BROKER_URL)
celery.send_task('detect_image', args=[{}])
```


## Build image from Dockerfile:
- cpu
```
docker build --no-cache -t penolove/keras_yolo_v3:cpu cpu/
```

- gpu
```
docker build --no-cache -t penolove/keras_yolo_v3:gpu gpu/
```
