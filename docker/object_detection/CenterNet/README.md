# CenterNet eyewitness example

https://github.com/penolove/CenterNet branch: eyeWitnessWrapper

this is a fork from https://github.com/xingyizhou/CenterNet , and a wrapper example with eyewitness 

## pre-requirement
1. make required folder
```
mkdir weights
mkdir detected_image
mkdir raw_image
mkdir db_folder  # used for end2end example
```

2. download pre-trained model [ctdet_pascal_dla_512](https://github.com/xingyizhou/CenterNet/blob/master/readme/MODEL_ZOO.md) into weights folder. (weights/ctdet_pascal_dla_512.pth)


3. nvidia-docker, docker-compose
https://github.com/NVIDIA/nvidia-docker
https://docs.docker.com/compose/install/

4. install nvidia-container-runtime and change the default runtime of docker container
https://github.com/NVIDIA/nvidia-container-runtime
since we need cuda runtime during Build image, we need to install nvidia-container-runtime
https://github.com/NVIDIA/nvidia-docker/issues/1033
```
sudo apt-get install nvidia-container-runtime
```

also set following in /etc/docker/daemon.json
```
{
    "runtimes": {
        "nvidia": {
            "path": "/usr/bin/nvidia-container-runtime",
            "runtimeArgs": []
        }
    },
    "default-runtime": "nvidia"
}
```
restart dockerd
```
sudo pkill -SIGHUP dockerd
```

## RUN up example

```
# run and access container
docker-compose -f gpu/docker-compose.yml build
docker-compose -f gpu/docker-compose.yml up -d
docker-compose -f gpu/docker-compose.yml exec centernet bash

# run naive_detector example, will store a famous boy-club image in the detected_image folder
PYTHONPATH=`pwd`/src python src/naive_detector.py --load_model models/ctdet_pascal_dla_512.pth --dataset pascal --arch dla_34

```
