import unittest
from pathlib import Path

from lxml import etree
from eyewitness.detection_utils import DetectionResult
from eyewitness.image_id import ImageId
from eyewitness.result_handler.db_writer import (
    BboxPeeweeDbWriter,
)
from eyewitness.config import (
    BoundedBoxObject,
    DETECTED_OBJECTS,
    DRAWN_IMAGE_PATH,
    RAW_IMAGE_PATH,
    IMAGE_ID,
)
from eyewitness.models.db_proxy import DATABASE_PROXY
from eyewitness.models.detection_models import (BboxDetectionResult, ImageInfo)
from eyewitness.dataset_util import generate_etree_obj, read_ori_anno_and_store_filered_result
from peewee import SqliteDatabase


class BboxDatasetTest(unittest.TestCase):
    def test_generate_etree_obj(self):
        database = SqliteDatabase(':memory:')
        # using memory db for testing
        bbox_sqlite_handler = BboxPeeweeDbWriter(database)

        image_dict = {
            IMAGE_ID: ImageId('channel1', 1541860141),
            DETECTED_OBJECTS: [
                BoundedBoxObject(*(1, 1, 2, 2, 'person', 0.5, '')),
                BoundedBoxObject(*(2, 2, 4, 4, 'person', 0.5, '')),
            ],
            DRAWN_IMAGE_PATH: 'drawn_minhan.jpg',
        }
        image_meta = {RAW_IMAGE_PATH: 'minhan.jpg'}
        detection_result = DetectionResult(image_dict)
        bbox_sqlite_handler.register_image(detection_result.image_id, image_meta)
        bbox_sqlite_handler.handle(detection_result)

        # test generate_etree_obj
        DATABASE_PROXY.initialize(database)
        valid_classes = set(['person'])

        dataset_name = 'unitest'
        valid_images = list(ImageInfo.select())
        self.assertEqual(len(valid_images), 1)
        valid_image = valid_images[0]
        detected_objects = list(BboxDetectionResult.select().where(
            BboxDetectionResult.image_id == valid_image,
            BboxDetectionResult.label.in_(valid_classes)))
        etree_obj = generate_etree_obj(valid_image.image_id, detected_objects, dataset_name)
        self.assertEqual(len(etree_obj.findall('object')), 2)

    def test_filter_anno_objs(self):
        source_file = str(
            Path('eyewitness', 'test', 'dataset', 'VOC2007_for_unitest', 'Annotations',
                 '000001.xml'))
        # check if the source with other than person correctly
        source_objs = set(i.find('name').text for i in etree.parse(source_file).findall('object'))
        self.assertEqual(set(['dog', 'person']), source_objs)

        dest_file_str = str(
            Path('eyewitness', 'test', 'dataset', 'VOC2007_for_unitest', '000001_unittest.xml'))
        valid_labels = set(['person'])
        read_ori_anno_and_store_filered_result(source_file, dest_file_str, valid_labels, False)
        dest_objs = set(i.find('name').text for i in etree.parse(dest_file_str).findall('object'))
        # check if only person left
        self.assertEqual(set(['person']), dest_objs)

        valid_labels = set(['minhan'])
        dest_file = Path('eyewitness', 'test', 'dataset', 'VOC2007_for_unitest',
                         '000001_unittest_no_obj.xml')
        if dest_file.exists():
            dest_file.unlink()
        read_ori_anno_and_store_filered_result(source_file, str(dest_file), valid_labels, True)
        self.assertEqual(dest_file.exists(), False)
