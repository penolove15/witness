import unittest
from pathlib import Path

import numpy as np
from eyewitness.image_utils import ImageHandler
from eyewitness.image_utils import swap_channel_rgb_bgr


class SwapRGBTest(unittest.TestCase):
    def test_image_swap(self):
        image_path = str(Path('eyewitness', 'test', 'pics', 'pikachu.png'))
        normal_pikachu = ImageHandler.read_image_file(image_path).convert('RGB')
        swap_channel_rgb_bgr(np.array(normal_pikachu))
