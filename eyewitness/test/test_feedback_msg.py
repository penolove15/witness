import unittest
import json

import arrow
from eyewitness.feedback_msg_utils import FeedbackMsg
from eyewitness.image_id import ImageId
from eyewitness.audience_id import AudienceId
from eyewitness.config import (
    BboxObjectFeedback,
    FEEDBACK_BBOX_OBJ,
    FEEDBACK_NO_OBJ,
    IMAGE_ID,
    AUDIENCE_ID,
    FEEDBACK_METHOD,
    FEEDBACK_MSG_OBJS,
    RECEIVE_TIME,
)


class FeedbackMsgTest(unittest.TestCase):
    def test_feedback_false_alert(self):
        # image_info
        date_str_format = 'YYYY-MM-DD HH:mm:ss'
        img_timestamp = arrow.get('2013-05-05 12:30:45', date_str_format).timestamp
        image_id1 = ImageId('channel1', img_timestamp, file_format='png')

        # feedback_dict
        audience_id = AudienceId('line', 'minhan_hgdfmjg2715')
        feedback_dict = {
            IMAGE_ID: image_id1,
            AUDIENCE_ID: audience_id,
            FEEDBACK_METHOD: FEEDBACK_NO_OBJ,
            FEEDBACK_MSG_OBJS: [],
            RECEIVE_TIME: img_timestamp,
        }

        feedback_msg = FeedbackMsg(feedback_dict)

        self.assertEqual(str(feedback_msg.image_id), 'channel1--%s--png' % str(img_timestamp))
        self.assertEqual(str(feedback_msg.audience_id), 'line--minhan_hgdfmjg2715')
        self.assertEqual(len(feedback_msg.feedback_msg_objs), 0)
        self.assertTrue(all(i for i in feedback_msg.feedback_msg_objs))

    def test_feedback_bbox(self):
        # image_info
        date_str_format = 'YYYY-MM-DD HH:mm:ss'
        img_timestamp = arrow.get('2013-05-05 12:30:45', date_str_format).timestamp
        image_id1 = ImageId('channel1', img_timestamp, file_format='png')

        # feedback_dict
        audience_id = AudienceId('line', 'minhan_hgdfmjg2715')
        feedback_dict = {
            IMAGE_ID: image_id1,
            AUDIENCE_ID: audience_id,
            FEEDBACK_METHOD: FEEDBACK_BBOX_OBJ,
            FEEDBACK_MSG_OBJS: [
                BboxObjectFeedback(*(1, 1, 100, 100, 'min-han', 'tiny')),
                BboxObjectFeedback(*(40, 60, 40, 60, 'small_min-han', 'BJ4')),
            ],
            RECEIVE_TIME: img_timestamp,
        }

        feedback_msg = FeedbackMsg(feedback_dict)
        self.assertEqual(str(feedback_msg.image_id), 'channel1--1367757045--png')
        self.assertEqual(str(feedback_msg.audience_id), 'line--minhan_hgdfmjg2715')
        self.assertEqual(len(feedback_msg.feedback_msg_objs), 2)

    def test_feedback_from_json(self):
        json_dict = {
            IMAGE_ID: str(ImageId('channel1', 1541860141, 'png')),
            AUDIENCE_ID: str(AudienceId('line', 'minhan_hgdfmjg2715')),
            FEEDBACK_METHOD: FEEDBACK_BBOX_OBJ,
            FEEDBACK_MSG_OBJS: [
                (1, 1, 100, 100, 'min-han', 'tiny'),
                (40, 60, 40, 60, 'small_min-han', 'BJ4'),
            ],
            RECEIVE_TIME: 1541860141,
        }
        json_str = json.dumps(json_dict)
        feedback_msg = FeedbackMsg.from_json(json_str)

        self.assertEqual(len(feedback_msg.feedback_msg_objs), 2)
        self.assertEqual(str(feedback_msg.image_id), 'channel1--1541860141--png')
        self.assertEqual(str(feedback_msg.audience_id), 'line--minhan_hgdfmjg2715')
        self.assertTrue(
            all(isinstance(i, BboxObjectFeedback) for i in feedback_msg.feedback_msg_objs))
