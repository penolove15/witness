import unittest

from eyewitness.detection_utils import DetectionResult
from eyewitness.image_id import ImageId
from eyewitness.result_handler.db_writer import (
    BboxNativeSQLiteDbWriter,
    BboxPeeweeDbWriter,
)
from eyewitness.config import (
    BoundedBoxObject,
    DETECTED_OBJECTS,
    DRAWN_IMAGE_PATH,
    RAW_IMAGE_PATH,
    IMAGE_ID,
)
from peewee import SqliteDatabase


class BboxResultDbHandlerTest(unittest.TestCase):
    def test_bbox_native_sqlite_writer(self):
        # using memory db for testing
        bbox_sqlite_handler = BboxNativeSQLiteDbWriter(':memory:')

        image_dict = {
            IMAGE_ID: ImageId('channel1', 1541860141),
            DETECTED_OBJECTS: [
                BoundedBoxObject(*(1, 1, 2, 2, 'person', 0.5, '')),
                BoundedBoxObject(*(2, 2, 4, 4, 'person', 0.5, '')),
            ],
            DRAWN_IMAGE_PATH: 'drawn_minhan.jpg',
        }
        image_meta = {RAW_IMAGE_PATH: 'minhan.jpg'}
        detection_result = DetectionResult(image_dict)
        bbox_sqlite_handler.register_image(detection_result.image_id, image_meta)
        bbox_sqlite_handler.handle(detection_result)

        # check insertion results
        conn = bbox_sqlite_handler.conn
        c = conn.cursor()
        c.execute("select * from ImageInfo")
        image_info = c.fetchall()
        c.execute("select x1, y1, x2, y2, label, score, meta from BboxDetectionResult")
        detected_results = c.fetchall()
        # content check
        self.assertEqual(set(detected_results), set(image_dict[DETECTED_OBJECTS]))
        self.assertEqual(len(image_info), 1)
        self.assertEqual(image_info[0],
                         ('channel1--1541860141--jpg', 1541860141, 'channel1', 'jpg',
                          image_meta[RAW_IMAGE_PATH], image_dict[DRAWN_IMAGE_PATH]))
        self.assertEqual(len(detected_results), 2)

    def test_bbox_peewee_sqlite_writer(self):
        database = SqliteDatabase(':memory:')
        # using memory db for testing
        bbox_sqlite_handler = BboxPeeweeDbWriter(database)

        image_dict = {
            IMAGE_ID: ImageId('channel1', 1541860141),
            DETECTED_OBJECTS: [
                BoundedBoxObject(*(1, 1, 2, 2, 'person', 0.5, '')),
                BoundedBoxObject(*(2, 2, 4, 4, 'person', 0.5, '')),
            ],
            DRAWN_IMAGE_PATH: 'drawn_minhan.jpg',
        }
        image_meta = {RAW_IMAGE_PATH: 'minhan.jpg'}
        detection_result = DetectionResult(image_dict)

        bbox_sqlite_handler.register_image(detection_result.image_id, image_meta)
        bbox_sqlite_handler.handle(detection_result)
        # check insertion results
        conn = bbox_sqlite_handler.database.connection()
        c = conn.cursor()
        c.execute("select * from ImageInfo")
        image_info = c.fetchall()
        c.execute("select x1, y1, x2, y2, label, score, meta from BboxDetectionResult")
        detected_results = c.fetchall()
        c.execute("select image_id from BboxDetectionResult")
        image_id_foregin_key = c.fetchall()

        # content check
        self.assertEqual(set(detected_results), set(image_dict[DETECTED_OBJECTS]))
        self.assertEqual(len(image_info), 1)
        self.assertEqual(image_info[0],
                         ('channel1--1541860141--jpg', 'channel1', 'jpg', 1541860141,
                          image_meta[RAW_IMAGE_PATH], image_dict[DRAWN_IMAGE_PATH]))
        self.assertEqual(len(detected_results), 2)
        self.assertEqual(set(j for i in image_id_foregin_key for j in i),
                         set(['channel1--1541860141--jpg']))

    def test_bbox_peewee_sqlite_writer_switch_db(self):
        # new connection to db with ':memory:' always create a new database
        database_1 = SqliteDatabase(':memory:')
        bbox_sqlite_handler_1 = BboxPeeweeDbWriter(database_1)
        database_2 = SqliteDatabase(':memory:')
        bbox_sqlite_handler_2 = BboxPeeweeDbWriter(database_2)

        image_dict = {
            IMAGE_ID: ImageId('channel1', 1541860141),
            DETECTED_OBJECTS: [
                BoundedBoxObject(*(1, 1, 2, 2, 'person', 0.5, '')),
                BoundedBoxObject(*(2, 2, 4, 4, 'person', 0.5, '')),
            ]
        }
        detection_result = DetectionResult(image_dict)

        # insert first db content
        bbox_sqlite_handler_1.register_image(detection_result.image_id, {})
        bbox_sqlite_handler_1.handle(detection_result)

        # insert second db content
        bbox_sqlite_handler_2.register_image(detection_result.image_id, {})
        bbox_sqlite_handler_2.handle(detection_result)

        # check first insertion results
        conn = bbox_sqlite_handler_1.database.connection()
        c = conn.cursor()
        c.execute("select * from ImageInfo")
        image_info = c.fetchall()
        c.execute("select x1, y1, x2, y2, label, score, meta from BboxDetectionResult")
        detected_results = c.fetchall()
        # content check
        self.assertEqual(set(detected_results), set(image_dict[DETECTED_OBJECTS]))
        self.assertEqual(len(image_info), 1)
        self.assertEqual(image_info[0],
                         ('channel1--1541860141--jpg', 'channel1', 'jpg', 1541860141, None, None))
        self.assertEqual(len(detected_results), 2)

        # check second insertion results
        conn = bbox_sqlite_handler_2.database.connection()
        c = conn.cursor()
        c.execute("select * from ImageInfo")
        image_info = c.fetchall()
        c.execute("select x1, y1, x2, y2, label, score, meta from BboxDetectionResult")
        detected_results = c.fetchall()

        # content check
        self.assertEqual(set(detected_results), set(image_dict[DETECTED_OBJECTS]))
        self.assertEqual(len(image_info), 1)
        self.assertEqual(image_info[0],
                         ('channel1--1541860141--jpg', 'channel1', 'jpg', 1541860141, None, None))
        self.assertEqual(len(detected_results), 2)

    def test_bbox_peewee_sqlite_writer_auto_image_registration(self):
        database = SqliteDatabase(':memory:')
        # using memory db for testing
        bbox_sqlite_handler = BboxPeeweeDbWriter(database, auto_image_registration=True)

        image_dict = {
            IMAGE_ID: ImageId('channel1', 1541860141),
            DETECTED_OBJECTS: [
                BoundedBoxObject(*(1, 1, 2, 2, 'person', 0.5, '')),
                BoundedBoxObject(*(2, 2, 4, 4, 'person', 0.5, '')),
            ],
            DRAWN_IMAGE_PATH: 'drawn_minhan.jpg',
        }
        detection_result = DetectionResult(image_dict)

        bbox_sqlite_handler.handle(detection_result)
        # check insertion results
        conn = bbox_sqlite_handler.database.connection()
        c = conn.cursor()
        c.execute("select * from ImageInfo")
        image_info = c.fetchall()
        c.execute("select x1, y1, x2, y2, label, score, meta from BboxDetectionResult")
        detected_results = c.fetchall()
        c.execute("select image_id from BboxDetectionResult")
        image_id_foregin_key = c.fetchall()

        # content check
        self.assertEqual(set(detected_results), set(image_dict[DETECTED_OBJECTS]))
        self.assertEqual(len(image_info), 1)
        self.assertEqual(image_info[0],
                         ('channel1--1541860141--jpg', 'channel1', 'jpg', 1541860141,
                          None, image_dict[DRAWN_IMAGE_PATH]))
        self.assertEqual(len(detected_results), 2)
        self.assertEqual(set(j for i in image_id_foregin_key for j in i),
                         set(['channel1--1541860141--jpg']))
