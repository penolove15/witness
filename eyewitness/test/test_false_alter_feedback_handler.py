import unittest

import arrow
from peewee import SqliteDatabase
from eyewitness.config import (
    FEEDBACK_NO_OBJ,
    IMAGE_ID,
    AUDIENCE_ID,
    FEEDBACK_METHOD,
    RECEIVE_TIME,
    FEEDBACK_META,
)
from eyewitness.result_handler.db_writer import FalseAlertPeeweeDbWriter
from eyewitness.audience_id import AudienceId
from eyewitness.feedback_msg_utils import FeedbackMsg
from eyewitness.image_id import ImageId


class FeedbackHandlerTest(unittest.TestCase):
    def test_false_alter_feedback_handler(self):
        # using memory db for testing
        db_obj = SqliteDatabase(':memory:')
        false_alert_feedback_handler = FalseAlertPeeweeDbWriter(db_obj)

        # insert registered_user
        audience_id = AudienceId(user_id='min-han', platform_id='line')
        false_alert_feedback_handler.register_audience(audience_id, {'description': 'handsome'})

        # insert image_info
        image_id = ImageId('channel1', 1541860141)
        arrive_time = arrow.now()
        false_alert_feedback_handler.register_image(image_id, {})

        feedback_dict = {
            AUDIENCE_ID: audience_id, RECEIVE_TIME: arrive_time.timestamp,
            FEEDBACK_METHOD: FEEDBACK_NO_OBJ, IMAGE_ID: image_id,
            FEEDBACK_META: 'mic_test'
        }
        feedback_obj = FeedbackMsg(feedback_dict)
        false_alert_feedback_handler.handle(feedback_obj)

        # check insertion results
        conn = false_alert_feedback_handler.database.connection()
        c = conn.cursor()
        c.execute("select user_id, platform_id, description from RegisteredAudience")
        registered_user = c.fetchall()
        c.execute("select image_id from ImageInfo")
        image_info = c.fetchall()
        c.execute("select audience_id, image_id, is_false_alert from FalseAlertFeedback")
        false_alert = c.fetchall()

        # content check
        self.assertEqual(len(registered_user), 1)
        self.assertEqual(registered_user[0], ('min-han', 'line', 'handsome'))
        self.assertEqual(len(image_info), 1)
        self.assertEqual(image_info[0][0], str(image_id))
        self.assertEqual(len(false_alert), 1)
        self.assertEqual(false_alert[0], ('line--min-han', 'channel1--1541860141--jpg', 1))
