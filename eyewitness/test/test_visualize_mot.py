import unittest
import random
from pathlib import Path

from eyewitness.mot.visualize_mot import draw_tracking_result
from eyewitness.mot.evaluation import VideoTrackedObjects
from typing import Sequence, Tuple

from eyewitness.mot.video import (
    FolderAsVideoData,
    FilesAsVideoData,
    Mp4AsVideoData,
)


def get_spaced_colors(n: int) -> Sequence[Tuple[int, int, int]]:
    """
    this function were used to generate color list, which can be used to generate color index
    since different object with different color can make the result detected result more clearly
    e.g
    >>> get_spaced_colors(3)
    [(0, 0, 0), (84, 86, 85), (168, 172, 170)]

    Parameters
    ----------
    n: int
        number of different colors

    Returns
    -------
    result: List[(int, int, int)]
        return List of color tuples
    """
    max_value = 16581375  # 255**3
    interval = int(max_value / n)
    colors = [hex(I)[2:].zfill(6) for I in range(0, max_value, interval)]
    return [(int(i[:2], 16), int(i[2:4], 16), int(i[4:], 16)) for i in colors]


class VisualizeMot(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.output_dest = Path(
            "eyewitness", "test", "mot_dataset", "MOT17-06-FRCNN", "output_dir_path",
        )
        cls.output_dest.mkdir(exist_ok=True, parents=True)

    def test_image_files_to_video(self):

        source_file_dir = Path(
            "eyewitness", "test", "mot_dataset", "MOT17-06-FRCNN", "imgs"
        )
        output_video_path = str(Path(self.output_dest, "images_to_video.mp4",))
        files_as_video = FilesAsVideoData(sorted(source_file_dir.glob("*.jpg")))
        files_as_video.to_video(output_video_path)

    def test_files_draw_tracking_result(self):
        color_list = get_spaced_colors(100)
        random.shuffle(color_list)

        mot_tracked_file = Path(
            "eyewitness",
            "test",
            "mot_dataset",
            "MOT17-06-FRCNN",
            "tracked_result",
            "MOT17-06-FRCNN.txt",
        )
        video_tracked_objects = VideoTrackedObjects.from_tracked_file(mot_tracked_file)

        source_file_dir = Path(
            "eyewitness", "test", "mot_dataset", "MOT17-06-FRCNN", "imgs"
        )
        files_as_video = FilesAsVideoData(sorted(source_file_dir.glob("*.jpg")))

        output_video_path = str(Path(self.output_dest, "result.mp4"))

        draw_tracking_result(
            video_tracked_objects,
            color_list,
            files_as_video,
            output_images_dir=str(self.output_dest),
            output_video_path=output_video_path,
        )

    def test_folder_draw_tracking_result(self):
        color_list = get_spaced_colors(100)
        random.shuffle(color_list)

        mot_tracked_file = Path(
            "eyewitness",
            "test",
            "mot_dataset",
            "MOT17-06-FRCNN",
            "tracked_result",
            "MOT17-06-FRCNN.txt",
        )
        video_tracked_objects = VideoTrackedObjects.from_tracked_file(mot_tracked_file)

        source_file_dir = Path(
            "eyewitness", "test", "mot_dataset", "MOT17-06-FRCNN", "imgs"
        )
        folder_as_video = FolderAsVideoData(source_file_dir)

        output_video_path = str(Path(self.output_dest, "result.mp4",))

        draw_tracking_result(
            video_tracked_objects,
            color_list,
            folder_as_video,
            output_images_dir=str(self.output_dest),
            output_video_path=output_video_path,
        )

    def test_mp4_in_mem_draw_tracking_result(self):
        color_list = get_spaced_colors(100)
        random.shuffle(color_list)

        mot_tracked_file = Path(
            "eyewitness",
            "test",
            "mot_dataset",
            "MOT17-06-FRCNN",
            "tracked_result",
            "MOT17-06-FRCNN.txt",
        )
        parsed_tracked_objects = VideoTrackedObjects.from_tracked_file(mot_tracked_file)

        mp4_file = Path(
            "eyewitness", "test", "mot_dataset", "MOT17-06-FRCNN", "video", "input.mp4"
        )
        mp4_as_video = Mp4AsVideoData(mp4_file)

        output_images_dir = str(
            Path(
                "eyewitness", "test", "mot_dataset", "MOT17-06-FRCNN", "output_dir_path"
            )
        )
        output_video_path = str(
            Path(
                "eyewitness",
                "test",
                "mot_dataset",
                "MOT17-06-FRCNN",
                "output_dir_path",
                "result.mp4",
            )
        )

        draw_tracking_result(
            parsed_tracked_objects,
            color_list,
            mp4_as_video,
            output_images_dir=output_images_dir,
            output_video_path=output_video_path,
        )

    def test_mp4_draw_tracking_result(self):
        color_list = get_spaced_colors(100)
        random.shuffle(color_list)

        mot_tracked_file = Path(
            "eyewitness",
            "test",
            "mot_dataset",
            "MOT17-06-FRCNN",
            "tracked_result",
            "MOT17-06-FRCNN.txt",
        )
        video_tracked_objects = VideoTrackedObjects.from_tracked_file(mot_tracked_file)

        mp4_file = str(
            Path(
                "eyewitness",
                "test",
                "mot_dataset",
                "MOT17-06-FRCNN",
                "video",
                "input.mp4",
            )
        )
        mp4_as_video = Mp4AsVideoData(mp4_file, in_memory=False)

        output_video_path = str(
            Path(
                "eyewitness",
                "test",
                "mot_dataset",
                "MOT17-06-FRCNN",
                "output_dir_path",
                "result.mp4",
            )
        )

        draw_tracking_result(
            video_tracked_objects,
            color_list,
            mp4_as_video,
            output_images_dir=str(self.output_dest),
            output_video_path=output_video_path,
        )
