import unittest

from eyewitness.config import (
    BBOX,
    BoundedBoxObject,
    IMAGE_ID,
    DETECTED_OBJECTS,
    DETECTION_METHOD
)
from eyewitness.detection_utils import DetectionResult
from eyewitness.image_utils import ImageHandler
from eyewitness.flask_server import BboxObjectDetectionFlaskWrapper
from eyewitness.object_detector import ObjectDetector
from eyewitness.result_handler.db_writer import BboxPeeweeDbWriter
from eyewitness.utils import bbox_intersection_over_union
from peewee import SqliteDatabase


class FakePikachuDetector(ObjectDetector):
    def __init__(self, enable_draw_bbox=True):
        self.enable_draw_bbox = enable_draw_bbox

    def detect(self, image_obj):
        """
        fake detect method for FakeObjDetector

        Parameters
        ----------
        image_obj: eyewitness.image_utils.Image

        Returns
        -------
        DetectionResult

        """
        image_dict = {
            IMAGE_ID: image_obj.image_id,
            DETECTED_OBJECTS: [
                BoundedBoxObject(*(15, 15, 250, 225, 'pikachu', 0.5, ''))
            ],
            DETECTION_METHOD: BBOX
        }
        if self.enable_draw_bbox:
            ImageHandler.draw_bbox(image_obj.pil_image_obj, image_dict[DETECTED_OBJECTS])

        detection_result = DetectionResult(image_dict)
        return detection_result


class FlaskServerTest(unittest.TestCase):
    def test_init_flask_server(self):
        database = SqliteDatabase(":memory:")
        fake_detector = FakePikachuDetector()
        result_handlers = []
        bbox_sqlite_handler = BboxPeeweeDbWriter(database)
        result_handlers.append(bbox_sqlite_handler)
        BboxObjectDetectionFlaskWrapper(
            fake_detector, bbox_sqlite_handler, result_handlers,
            database=database)

    def test_bbox_iou(self):
        out = bbox_intersection_over_union([1, 1, 2, 2], [1, 1, 2, 2])
        self.assertAlmostEqual(out, 1.0)
