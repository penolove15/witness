import unittest
from pathlib import Path

from eyewitness.mot.evaluation import mot_evaluation, VideoTrackedObjects


class MotEvaluation(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.output_dest = Path(
            "eyewitness", "test", "mot_dataset", "MOT17-05-FRCNN", "output_dir_path",
        )
        cls.output_dest.mkdir(exist_ok=True, parents=True)

    def test_mot_evaluation(self):
        gt_file = str(
            Path("eyewitness", "test", "mot_dataset", "MOT17-05-FRCNN", "gt", "gt.txt")
        )
        tracked_file = str(
            Path(
                "eyewitness",
                "test",
                "mot_dataset",
                "MOT17-05-FRCNN",
                "tracked_result",
                "MOT17-05-FRCNN.txt",
            )
        )
        video_gt_objects = VideoTrackedObjects.from_tracked_file(
            gt_file, ignore_gt_flag=True
        )
        video_tracked_objects = VideoTrackedObjects.from_tracked_file(tracked_file)
        mot_evaluation(video_gt_objects, video_tracked_objects)

    def test_from_dict(self):
        tracked_file = str(
            Path(
                "eyewitness",
                "test",
                "mot_dataset",
                "MOT17-05-FRCNN",
                "tracked_result",
                "MOT17-05-FRCNN.txt",
            )
        )

        video_tracked_objects = VideoTrackedObjects.from_tracked_file(tracked_file)
        result = VideoTrackedObjects.from_dict(video_tracked_objects)
        self.assertDictEqual(result, video_tracked_objects)

    def test_to_file(self):
        tracked_file = str(
            Path(
                "eyewitness",
                "test",
                "mot_dataset",
                "MOT17-05-FRCNN",
                "tracked_result",
                "MOT17-05-FRCNN.txt",
            )
        )

        output_tracked_file = str(Path(self.output_dest, "tracked_file.csv",))
        video_tracked_objects = VideoTrackedObjects.from_tracked_file(tracked_file)
        video_tracked_objects.to_file(output_tracked_file)
        written_video_tracked_objects = VideoTrackedObjects.from_tracked_file(
            output_tracked_file
        )

        self.assertSetEqual(
            set(video_tracked_objects.keys()), set(written_video_tracked_objects.keys())
        )

        self.assertTrue(
            all(
                len(video_tracked_objects[i]) == len(written_video_tracked_objects[i])
                for i in video_tracked_objects
            )
        )

        # number of objects
        n_total_objs = sum(len(i) for i in video_tracked_objects.values())
        n_written_total_objs = sum(
            len(i) for i in written_video_tracked_objects.values()
        )
        self.assertEqual(n_total_objs, n_written_total_objs)
