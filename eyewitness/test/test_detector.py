import unittest
from pathlib import Path

from eyewitness.config import (
    BBOX,
    BoundedBoxObject,
    IMAGE_ID,
    DETECTED_OBJECTS,
    DETECTION_METHOD
)
from eyewitness.detection_utils import DetectionResult
from eyewitness.image_utils import Image, ImageHandler
from eyewitness.image_id import ImageId
from eyewitness.object_detector import ObjectDetector


class FakePikachuDetector(ObjectDetector):
    def __init__(self, enable_draw_bbox=True):
        self.enable_draw_bbox = enable_draw_bbox

    def detect(self, image_obj):
        """
        fake detect method for FakeObjDetector

        Parameters
        ----------
        image_obj: eyewitness.image_utils.Image

        Returns
        -------
        DetectionResult

        """
        image_dict = {
            IMAGE_ID: image_obj.image_id,
            DETECTED_OBJECTS: [
                BoundedBoxObject(*(250, 100, 800, 900, 'pikachu', 0.5, ''))
            ],
            DETECTION_METHOD: BBOX
        }

        detection_result = DetectionResult(image_dict)
        return detection_result


class DetectorTest(unittest.TestCase):
    def test_detector(self):
        image_path = str(Path('eyewitness', 'test', 'pics', 'pikachu.png'))
        normal_pikachu = ImageHandler.read_image_file(image_path).convert('RGB')
        image_id = ImageId(channel='pikachu', timestamp=12345678, file_format='png')
        image_obj = Image(image_id, pil_image_obj=normal_pikachu)
        fake_detector = FakePikachuDetector()
        fake_detector.detect(image_obj)
        image_id = ImageId(channel='pikachu', timestamp=12345678, file_format='png')
        image_obj = Image(image_id, raw_image_path=image_path)
        fake_detector.detect(image_obj)
