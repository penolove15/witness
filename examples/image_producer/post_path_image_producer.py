import arrow
from eyewitness.image_utils import PostFilePathImageProducer
from eyewitness.image_id import ImageId


if __name__ == '__main__':
    image_producer = PostFilePathImageProducer(host='localhost:5566')
    image_id = ImageId(channel='pikachu', timestamp=arrow.now().timestamp, file_format='png')
    image_producer.produce_image(
        image_id=image_id,
        raw_image_path='eyewitness/test/pics/pikachu.png')
