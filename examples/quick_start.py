import time
from pathlib import Path

import arrow
from eyewitness.config import IN_MEMORY
from eyewitness.image_utils import ImageHandler, ImageProducer
from eyewitness.object_detector import ObjectDetector
from eyewitness.detection_utils import DetectionResult
from eyewitness.config import (
    BBOX,
    BoundedBoxObject,
    DRAWN_IMAGE_PATH,
    IMAGE_ID,
    DETECTED_OBJECTS,
    DETECTION_METHOD
)
from eyewitness.result_handler.db_writer import BboxPeeweeDbWriter
from eyewitness.image_id import ImageId
from eyewitness.image_utils import Image
from peewee import SqliteDatabase


class InMemoryImageProducer(ImageProducer):
    def __init__(self, image_path, interval_s=3):
        self.pikachu = ImageHandler.read_image_file(image_path)
        self.interval_s = interval_s

    def produce_method(self):
        return IN_MEMORY

    def produce_image(self):
        while True:
            yield self.pikachu
            time.sleep(self.interval_s)


class FakePikachuDetector(ObjectDetector):
    def __init__(self, enable_draw_bbox=True):
        self.enable_draw_bbox = enable_draw_bbox

    def detect(self, image_obj):
        """
        fake detect method for FakeObjDetector

        Parameters
        ----------
        image_obj: eyewitness.image_utils.Image

        Returns
        -------
        DetectionResult

        """
        image_dict = {
            IMAGE_ID: image_obj.image_id,
            DETECTED_OBJECTS: [
                BoundedBoxObject(*(15, 15, 250, 225, 'pikachu', 0.5, ''))
            ],
            DETECTION_METHOD: BBOX
        }
        if self.enable_draw_bbox:
            image_dict[DRAWN_IMAGE_PATH] = str(
                Path(Path.cwd(), '%s_out.png' % image_obj.image_id))
            ImageHandler.draw_bbox(image_obj.pil_image_obj, image_dict[DETECTED_OBJECTS])
            ImageHandler.save(image_obj.pil_image_obj, image_dict[DRAWN_IMAGE_PATH])
            print('draw image: %s' % image_dict[DRAWN_IMAGE_PATH])

        detection_result = DetectionResult(image_dict)
        return detection_result


if __name__ == '__main__':
    # init InMemoryImageProducer
    image_producer = InMemoryImageProducer('pikachu.png')

    # init FakePikachuDetector
    object_detector = FakePikachuDetector()

    # prepare detection result handler
    database = SqliteDatabase("example.sqlite")
    bbox_sqlite_handler = BboxPeeweeDbWriter(database)

    for image in image_producer.produce_image():
        # generate the image_id
        image_id = ImageId(channel='pikachu', timestamp=arrow.now().timestamp, file_format='png')
        image_obj = Image(image_id, pil_image_obj=image)

        bbox_sqlite_handler.register_image(image_id, {})  # register the image_info
        detection_result = object_detector.detect(image_obj)
        bbox_sqlite_handler.handle(detection_result)    # insert detection bbox result
